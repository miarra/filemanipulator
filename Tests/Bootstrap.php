<?php declare(strict_types = 1);

namespace Tests;

require_once __DIR__ . '/../vendor/autoload.php';

use Tester\Environment;
use Tester\Helpers;
use function date_default_timezone_set;
use function define;
use function getmypid;
use function mkdir;

define('TEMP_DIR', Bootstrap::TMP_DIR . '/' . getmypid());

class Bootstrap
{

	public const
		TMP_DIR = __DIR__ . '/../temp/tests';

	public static function boot(): void
	{
		Environment::setup();
		date_default_timezone_set('Europe/Prague');
		@mkdir(self::TMP_DIR);
	}

	public static function purge(): void
	{
		Helpers::purge(TEMP_DIR);
	}

}

Bootstrap::boot();
