<?php declare(strict_types = 1);

namespace Tests\Miarra\FileManipulator;

require_once __DIR__ . '/../Bootstrap.php';

use CURLFile;
use Miarra\FileManipulator\CURLBuilder;
use Miarra\FileManipulator\CURLProxy;
use Miarra\FileManipulator\FileManipulator;
use Miarra\FileManipulator\FileUploadHolder;
use Mockery;
use Nette\Http\FileUpload;
use Nette\Http\IRequest as Request;
use Nette\InvalidArgumentException;
use Nette\Schema\ValidationException;
use Tester\Assert;
use Tester\Environment;
use Tester\TestCase;
use Throwable;
use function array_map;
use function assert;
use const CURLOPT_CUSTOMREQUEST;
use const CURLOPT_HTTPHEADER;
use const CURLOPT_POSTFIELDS;
use const CURLOPT_RETURNTRANSFER;
use const CURLOPT_URL;
use const CURLOPT_USERPWD;

Environment::bypassFinals();

class FileManipulatorTest extends TestCase
{

	private string $url = 'url.cz/';

	private string $username = 'un';

	private string $password = 'pw';

	protected function tearDown(): void
	{
		parent::tearDown();
		Mockery::close();
	}

	/**
	 * @param array<mixed> $curlProxyArgs
	 * @param array<mixed> $fileUploadArgs
	 * @param array<string> $expected
	 * @dataProvider saveWithoutExceptionsArgs
	 */
	public function testSaveWithoutExceptions(
		array $curlProxyArgs,
		array $fileUploadArgs,
		array $expected,
	): void
	{
		$curlProxyMock = $this->getCURLProxyMock(...$curlProxyArgs);

		$fileUploadHolderData = array_map(function (array $value) {
			$value['file'] = $this->getFileUploadMock(...$value['file']);

			return $value;
		}, $fileUploadArgs);

		// @phpstan-ignore-next-line
		$fileUploadHolder = $this->getFileUploadHolderMock($fileUploadHolderData, []);

		$fileManipulator = new FileManipulator(
			$curlProxyMock,
			$this->url,
			$this->username,
			$this->password,
		);

		Assert::equal(
			$expected,
			$fileManipulator->saveImages($fileUploadHolder),
		);
	}

	/**
	 * @return array<array<mixed>>
	 */
	public function saveWithoutExceptionsArgs(): array
	{
		$curlFile = $this->getCurlFileMock();

		return [
			[ // case 1: 1 picture without path
				'curlProxyArgs' => [
					'createCurlFileArgs' => [
						'firstImage' => [
							'tempFile', // FileUpload::getTemporaryFile()
							'jpeg', // FileUpload::getContentType()
							'img1.jpg', // FileUpload::getUntrustedName()
						],
					],
					'createCurlFileRetVal' => $curlFile,
					'setOptTimes' => 1, // 1 if save should not end with InvalidArgumentException 0 otherwise
					'setOptUrl' => $this->url . '/images/save',
					'setOptCredentials' => $this->username . ':' . $this->password,
					'setOptMethod' => Request::POST, // For save always POST
					'setOptPostFields' => [
						'upload_file_0' => $curlFile, //Mockery::mock(CURLFile::class)
						'00493b3f5af40f5e614214702e49cd8f' => 'img1.jpg',
					],
					'execRetVal' => '{"state":"SUCCESS","filepath":["/img1.jpg"]}',
				],
				'fileUploadArgs' => [ // Return value of FileUploadHolder::getContent
					[
						'file' => [
							'name' => 'img1.jpg',
							'tempFile' => 'tempFile',
							'contentType' => 'jpeg',
						],
						'filePath' => 'img1.jpg',
					],
				],
				'expected' => [
					'/img1.jpg',
				],
			],
			[ // case 2: 2 pictures without path
				'curlProxyArgs' => [
					'createCurlFileArgs' => [
						'firstImage' => [
							'tempFile', // FileUpload::getTemporaryFile()
							'jpeg', // FileUpload::getContentType()
							'img1.jpg', // FileUpload::getUntrustedName()
						],
						'secondImage' => [
							'tempFile2', // FileUpload::getTemporaryFile()
							'png', // FileUpload::getContentType()
							'img2.png', // FileUpload::getUntrustedName()
						],
					],
					'createCurlFileRetVal' => $curlFile,
					'setOptTimes' => 1, // 1 if save should not end with InvalidArgumentException 0 otherwise
					'setOptUrl' => $this->url . '/images/save',
					'setOptCredentials' => $this->username . ':' . $this->password,
					'setOptMethod' => Request::POST, // For save always POST
					'setOptPostFields' => [
						'upload_file_0' => $curlFile, //Mockery::mock(CURLFile::class)
						'00493b3f5af40f5e614214702e49cd8f' => 'img1.jpg',
						'upload_file_1' => $curlFile, //Mockery::mock(CURLFile::class)
						'65bd2c9b931f057d7307dfaaa8d5c433' => 'img2.png',
					],
					'execRetVal' => '{"state":"SUCCESS","filepath":["/img1.jpg", "/img2.png"]}',
				],
				'fileUploadArgs' => [ // Return value of FileUploadHolder::getContent
					[
						'file' => [
							'name' => 'img1.jpg',
							'tempFile' => 'tempFile',
							'contentType' => 'jpeg',
						],
						'filePath' => 'img1.jpg',
					],
					[
						'file' => [
							'name' => 'img2.png',
							'tempFile' => 'tempFile2',
							'contentType' => 'png',
						],
						'filePath' => 'img2.png',
					],
				],
				'expected' => [
					'/img1.jpg',
					'/img2.png',
				],
			],
		];
	}

	/**
	 * @param array<FileUpload> $containsOnlyImagesRetVal
	 * @dataProvider saveWithExceptionInvalidArg
	 */
	public function testSaveWithExceptionInvalidArg(
		array $containsOnlyImagesRetVal,
		string $errorMessage,
	): void
	{
		$curlProxyMock = $this->getCURLProxyMock(
			$this->getCurlFileMock(),
		);

		$fileUploadHolder = $this->getFileUploadHolderMock(
			[],
			$containsOnlyImagesRetVal,
		);

		$fileManipulator = new FileManipulator(
			$curlProxyMock,
			$this->url,
			$this->username,
			$this->password,
		);

		Assert::exception(static function () use ($fileManipulator, $fileUploadHolder): void {
			// @phpstan-ignore-next-line
			$fileManipulator->saveImages($fileUploadHolder);
		}, InvalidArgumentException::class, $errorMessage, 654_111);
	}

	/**
	 * @return array<array<array<string>|string>>
	 */
	public function saveWithExceptionInvalidArg(): array
	{
		return [
			[
				'containsOnlyImagesRetVal' => [
					'image123.jpg' => 'someString',
				],
				'errorMessage' => 'Function SaveImages accept only array of images. "image123.jpg".',
			],
			[
				'containsOnlyImagesRetVal' => [
					'image123.jpg' => 'someString',
					'image1234.jpg' => 'someString',
				],
				'errorMessage' => 'Function SaveImages accept only array of images. "image123.jpg", "image1234.jpg".',
			],
		];
	}

	public function testSaveWithExceptionFromResponse(): void
	{
		$curlFile = $this->getCurlFileMock();

		$curlProxyMock = $this->getCURLProxyMock(
			createCurlFileRetVal: $curlFile,
			createCurlFileArgs: [
				'firstImage' => [
					'tempFile', // FileUpload::getTemporaryFile()
					'jpeg', // FileUpload::getContentType()
					'img1.jpg', // FileUpload::getUntrustedName()
				],
			],
			setOptTimes: 1, // 1 if save should not end with InvalidArgumentException 0 otherwise
			setOptUrl: $this->url . '/images/save',
			setOptCredentials: $this->username . ':' . $this->password,
			setOptMethod: Request::POST, // For save always POST
			setOptPostFields: [
				'upload_file_0' => $curlFile, //Mockery::mock(CURLFile::class)
				'00493b3f5af40f5e614214702e49cd8f' => 'img1.jpg',
			],
			execRetVal: '{"state":"ERROR","message":"Looks like error.","code":222}',
		);

		$fileManipulator = new FileManipulator(
			$curlProxyMock,
			$this->url,
			$this->username,
			$this->password,
		);

		$fileUploadHolder = $this->getFileUploadHolderMock(
			fileUploadHolderContent: [
				[
					'file' => $this->getFileUploadMock(
						name: 'img1.jpg',
						tempFile: 'tempFile',
						contentType: 'jpeg',
					),
					'filePath' => 'img1.jpg',
				],
			],
			containsOnlyImagesRetVal: [],
		);

		Assert::exception(static function () use ($fileManipulator, $fileUploadHolder): void {
			$fileManipulator->saveImages($fileUploadHolder);
		}, Throwable::class, 'Looks like error.', 222);
	}

	/**
	 * @dataProvider deleteWithoutExceptionsArgs
	 * @param array<string>|string $imageArg
	 */
	public function testDeleteWithoutExceptions(
		array|string $imageArg,
		string $postFields,
		string $url,
		string $requestType,
		string $methodName,
	): void
	{
		$curlFile = $this->getCurlFileMock();

		$curlProxyMock = $this->getCURLProxyMock(
			createCurlFileRetVal: $curlFile,
			setOptTimes: 1, // 1 if save should not end with InvalidArgumentException 0 otherwise
			setOptUrl: $this->url . $url,
			setOptCredentials: $this->username . ':' . $this->password,
			setOptMethod: $requestType, // For save always POST
			setOptHeader: ['Content-Type: application/json;'],
			setOptPostFields: $postFields,
			execRetVal: '{"state":"SUCCESS","filepath":["img1.jpg"]}',
		);

		$fileManipulator = new FileManipulator(
			$curlProxyMock,
			$this->url,
			$this->username,
			$this->password,
		);

		Assert::same([
			'img1.jpg',
		], $fileManipulator->$methodName($imageArg));
	}

	/**
	 * @return array<array<mixed>>
	 */
	public function deleteWithoutExceptionsArgs(): array
	{
		return [
			[
				'imageArg' => 'img1.jpg',
				'postFields' => '["img1.jpg"]',
				'url' => '/images/delete',
				'requestType' => Request::DELETE,
				'methodName' => 'deleteImages',
			],
			[
				'imageArg' => [
					'img1.jpg',
				],
				'postFields' => '["img1.jpg"]',
				'url' => '/images/delete',
				'requestType' => Request::DELETE,
				'methodName' => 'deleteImages',
			],
			[
				'imageArg' => [
					'img1.jpg',
					'img2.jpg',
				],
				'postFields' => '["img1.jpg","img2.jpg"]',
				'url' => '/images/delete',
				'requestType' => Request::DELETE,
				'methodName' => 'deleteImages',
			],
			[
				'imageArg' => 'img1.jpg',
				'postFields' => '["img1.jpg"]',
				'url' => '/images/copy',
				'requestType' => Request::POST,
				'methodName' => 'copyImages',
			],
			[
				'imageArg' => [
					'img1.jpg',
				],
				'postFields' => '["img1.jpg"]',
				'url' => '/images/copy',
				'requestType' => Request::POST,
				'methodName' => 'copyImages',
			],
			[
				'imageArg' => [
					'img1.jpg',
					'img2.jpg',
				],
				'postFields' => '["img1.jpg","img2.jpg"]',
				'url' => '/images/copy',
				'requestType' => Request::POST,
				'methodName' => 'copyImages',
			],
		];
	}

	/**
	 * @dataProvider deleteWithExceptionFromResponseArgs
	 */
	public function testDeleteWithExceptionFromResponse(
		string $url,
		string $requestType,
		string $methodName,
	): void
	{
		$curlFile = $this->getCurlFileMock();

		$curlProxyMock = $this->getCURLProxyMock(
			createCurlFileRetVal: $curlFile,
			setOptTimes: 1, // 1 if save should not end with InvalidArgumentException 0 otherwise
			setOptUrl: $this->url . $url,
			setOptCredentials: $this->username . ':' . $this->password,
			setOptMethod: $requestType, // For save always POST
			setOptHeader: ['Content-Type: application/json;'],
			setOptPostFields: '["img1.jpg"]',
			execRetVal: '{"state":"ERROR","message":"Looks like error.","code":222}',
		);

		$fileManipulator = new FileManipulator(
			$curlProxyMock,
			$this->url,
			$this->username,
			$this->password,
		);

		Assert::exception(static function () use ($fileManipulator, $methodName): void {
			$fileManipulator->$methodName('img1.jpg');
		}, Throwable::class, 'Looks like error.', 222);
	}

	/**
	 * @return array<array<mixed>>
	 */
	public function deleteWithExceptionFromResponseArgs(): array
	{
		return [
			[
				'url' => '/images/delete',
				'requestType' => Request::DELETE,
				'methodName' => 'deleteImages',
			],
			[
				'url' => '/images/copy',
				'requestType' => Request::POST,
				'methodName' => 'copyImages',
			],
		];
	}

	/**
	 * @dataProvider deleteWithExceptionInvalidArgArgs
	 */
	public function testDeleteWithExceptionInvalidArg(string $methodName,): void
	{
		$curlFile = $this->getCurlFileMock();

		$curlProxyMock = $this->getCURLProxyMock(
			createCurlFileRetVal: $curlFile,
		);

		$fileManipulator = new FileManipulator(
			$curlProxyMock,
			$this->url,
			$this->username,
			$this->password,
		);

		Assert::exception(static function () use ($fileManipulator, $methodName): void {
			$fileManipulator->$methodName([123]);
		}, ValidationException::class, 'The item \'0\' expects to be string, 123 given.', 0);
	}

	/**
	 * @return array<array<mixed>>
	 */
	public function deleteWithExceptionInvalidArgArgs(): array
	{
		return [
			[
				'methodName' => 'deleteImages',
			],
			[
				'methodName' => 'copyImages',
			],
		];
	}

	public function testCreateFileUploadHolder(): void
	{
		$curlFile = $this->getCurlFileMock();

		$curlProxyMock = $this->getCURLProxyMock(
			createCurlFileRetVal: $curlFile,
		);

		$fileManipulator = new FileManipulator(
			$curlProxyMock,
			$this->url,
			$this->username,
			$this->password,
		);

		Assert::type(FileUploadHolder::class, $fileManipulator->createFileUploadHolder());
	}

	////// MOCKS:

	private function getCurlFileMock(): CURLFile
	{
		$retVal = Mockery::mock(CURLFile::class);
		assert($retVal instanceof CURLFile);

		return $retVal;
	}

	/**
	 * @param array<int, array<string, FileUpload|string>> $fileUploadHolderContent
	 * @param array<FileUpload> $containsOnlyImagesRetVal
	 */
	private function getFileUploadHolderMock(
		array $fileUploadHolderContent,
		array $containsOnlyImagesRetVal,
	): FileUploadHolder
	{
		$retVal = Mockery::mock(FileUploadHolder::class)
			->shouldReceive('containsOnlyImages')
			->once()
			->with()
			->andReturn($containsOnlyImagesRetVal)
			->shouldReceive('getContent')
			->times((int) !empty($fileUploadHolderContent))
			->with()
			->andReturn($fileUploadHolderContent)
			->getMock();
		assert($retVal instanceof FileUploadHolder);

		return $retVal;
	}

	private function getFileUploadMock(
		string $name,
		string $tempFile,
		string $contentType,
	): FileUpload
	{
		$retVal = Mockery::mock(FileUpload::class)
			->shouldReceive('getUntrustedName')
			->once()
			->with()
			->andReturn($name)
			->shouldReceive('getTemporaryFile')
			->once()
			->with()
			->andReturn($tempFile)
			->shouldReceive('getContentType')
			->once()
			->with()
			->andReturn($contentType)
			->getMock();

		assert($retVal instanceof FileUpload);

		return $retVal;
	}

	/**
	 * @param array<array<string>> $createCurlFileArgs
	 * @param array<string> $setOptHeader
	 * @param array<string, mixed>|string $setOptPostFields
	 */
	private function getCURLProxyMock(
		CURLFile $createCurlFileRetVal,
		array $createCurlFileArgs = [],
		int $setOptTimes = 0,
		string $setOptUrl = '',
		string $setOptCredentials = '',
		string $setOptMethod = '',
		array $setOptHeader = [],
		array|string $setOptPostFields = [],
		string $execRetVal = '',
	): CURLProxy
	{
		$retVal = Mockery::mock(CURLProxy::class)
			->shouldReceive('createCURLFile')
			->with(...($createCurlFileArgs['firstImage'] ?? []))
			->times((int) isset($createCurlFileArgs['firstImage']))
			->andReturn($createCurlFileRetVal)
			->shouldReceive('createCURLFile')
			->with(...($createCurlFileArgs['secondImage'] ?? []))
			->times((int) isset($createCurlFileArgs['secondImage']))
			->andReturn($createCurlFileRetVal)
			->shouldReceive('init')
			->with()
			->times($setOptTimes)
			->andReturn($this->getCURLBuilderMock(
				$setOptTimes,
				$setOptUrl,
				$setOptCredentials,
				$setOptMethod,
				$setOptHeader,
				$setOptPostFields,
				$execRetVal,
			))
			->getMock();

		assert($retVal instanceof CURLProxy);

		return $retVal;
	}

	/**
	 * @param array<string> $setOptHeader
	 * @param array<string, mixed>|string $setOptPostFields
	 */
	private function getCURLBuilderMock(
		int $setOptTimes,
		string $setOptUrl,
		string $setOptCredentials,
		string $setOptMethod,
		array $setOptHeader,
		array|string $setOptPostFields,
		string $execRetVal,
	): CURLBuilder
	{
		$retVal = Mockery::mock(CURLBuilder::class)
			->shouldReceive('setopt')
			->with(CURLOPT_URL, $setOptUrl)
			->times($setOptTimes)
			->andReturn(Mockery::self())
			->shouldReceive('setopt')
			->with(CURLOPT_USERPWD, $setOptCredentials)
			->times($setOptTimes)
			->andReturn(Mockery::self())
			->shouldReceive('setopt')
			->with(CURLOPT_CUSTOMREQUEST, $setOptMethod)
			->times($setOptTimes)
			->andReturn(Mockery::self())
			->shouldReceive('setopt')
			->with(CURLOPT_RETURNTRANSFER, true)
			->times($setOptTimes)
			->andReturn(Mockery::self())
			->shouldReceive('setopt')
			->with(CURLOPT_HTTPHEADER, $setOptHeader)
			->times((int) !empty($setOptHeader))
			->andReturn(Mockery::self())
			->shouldReceive('setopt')
			->with(CURLOPT_POSTFIELDS, $setOptPostFields)
			->times((int) !empty($setOptPostFields))
			->andReturn(Mockery::self())
			->shouldReceive('exec')
			->with()
			->times($setOptTimes)
			->andReturn($execRetVal)
			->shouldReceive('close')
			->with()
			->times($setOptTimes)
			->andReturn(null)
			->getMock();

		assert($retVal instanceof CURLBuilder);

		return $retVal;
	}

}

(new FileManipulatorTest())->run();
