<?php declare(strict_types = 1);

namespace Tests\Miarra\FileManipulator;

require_once __DIR__ . '/../Bootstrap.php';

use Miarra\FileManipulator\FileUploadHolder;
use Mockery;
use Nette\Http\FileUpload;
use Nette\InvalidArgumentException;
use Nette\Utils\DateTime;
use Tester\Assert;
use Tester\Environment;
use Tester\TestCase;
use function assert;

Environment::bypassFinals();

class FileUploadHolderTest extends TestCase
{

	private FileUploadHolder $fileUploadHolder;

	public function __construct()
	{
		$this->fileUploadHolder = new FileUploadHolder();
	}

	public function setUp(): void
	{
		parent::setUp();
		$this->fileUploadHolder->clear();
	}

	public function tearDown(): void
	{
		parent::tearDown();
		Mockery::close();
	}

	/**
	 * @dataProvider addFileUploadArgs
	 * @param array<string> $fileUploadMockParams
	 * @param array<array<FileUpload, string>> $expected
	 */
	public function testAddFileUpload(
		array $fileUploadMockParams,
		string $filePath,
		bool $autogeneratePathPrefix,
		array $expected,
	): void
	{
		$fileUpload = $this->getFileUploadMock(...$fileUploadMockParams);
		$expected[$fileUploadMockParams['getUntrustedNameRetVal']]['file'] = $fileUpload;

		$this->fileUploadHolder->addFileUpload(
			file: $fileUpload,
			filePath: $filePath,
			autogeneratePathPrefix: $autogeneratePathPrefix,
		);

		Assert::equal(
			$expected,
			$this->fileUploadHolder->getContent(),
		);
	}

	/**
	 * @return array<array<mixed>>
	 */
	public function addFileUploadArgs(): array
	{
		$fileUploadMockParams = [
			'getUntrustedNameRetVal' => 'img1.jpg',
			'isImageRetVal' => true,
			'getUntrustedNameTimes' => 1,
			'isImageTimes' => 0,
		];

		return [
			[ // case 1: Image without path and without auto-generated path
				'fileUploadMockParams' => $fileUploadMockParams,
				'filePath' => '',
				'autogeneratePathPrefix' => false,
				'expected' => [
					'img1.jpg' => [
						'file' => '',
						'filePath' => 'img1.jpg',
					],
				],
			],
			[ // case 2: Image with path and without auto-generated path
				'fileUploadMockParams' => $fileUploadMockParams,
				'filePath' => '/2021/07/17/b/image1.jpg',
				'autogeneratePathPrefix' => false,
				'expected' => [
					'img1.jpg' => [
						'file' => '',
						'filePath' => '/2021/07/17/b/image1.jpg',
					],
				],
			],
			[ // case 3: Image without path and with auto-generated path
				'fileUploadMockParams' => $fileUploadMockParams,
				'filePath' => '',
				'autogeneratePathPrefix' => true,
				'expected' => [
					'img1.jpg' => [
						'file' => '',
						'filePath' => DateTime::from('now')->format('/Y/m/d/') . 'img1.jpg',
					],
				],
			],
			[ // case 4: Image with path and with auto-generated path
				'fileUploadMockParams' => $fileUploadMockParams,
				'filePath' => '/a/b/c/123.jpg',
				'autogeneratePathPrefix' => true,
				'expected' => [
					'img1.jpg' => [
						'file' => '',
						'filePath' => DateTime::from('now')->format('/Y/m/d/') . '/a/b/c/123.jpg',
					],
				],
			],
		];
	}

	/**
	 * @dataProvider addFileUploadUniqueFileNameExceptionArgs
	 * @param array<string> $fileUploadMockParams
	 */
	public function testAddFileUploadUniqueFileNameException(
		array $fileUploadMockParams,
		string $filePath1,
		bool $autogeneratePathPrefix1,
		string $filePath2,
		bool $autogeneratePathPrefix2,
	): void
	{
		$fileUpload = $this->getFileUploadMock(...$fileUploadMockParams);
		$fileUpload2 = $this->getFileUploadMock(...$fileUploadMockParams);

		$this->fileUploadHolder->addFileUpload(
			file: $fileUpload,
			filePath: $filePath1,
			autogeneratePathPrefix: $autogeneratePathPrefix1,
		);

		$fileUploadHolder = $this->fileUploadHolder;

		Assert::exception(
			static function () use ($fileUploadHolder, $fileUpload2, $filePath2, $autogeneratePathPrefix2): void {
				$fileUploadHolder->addFileUpload(
					file: $fileUpload2,
					filePath: $filePath2,
					autogeneratePathPrefix: $autogeneratePathPrefix2,
				);
			},
			InvalidArgumentException::class,
			'Files must have unique name.',
			516_154,
		);

	}

	/**
	 * @return array<array<mixed>>
	 */
	public function addFileUploadUniqueFileNameExceptionArgs(): array
	{
		$fileUploadMockParams = [
			'getUntrustedNameRetVal' => 'img1.jpg',
			'isImageRetVal' => true,
			'getUntrustedNameTimes' => 1,
			'isImageTimes' => 0,
		];

		return [
			[
				'fileUploadMockParams' => $fileUploadMockParams,
				'filePath1' => '',
				'autogeneratePathPrefix1' => false,
				'filePath2' => '',
				'autogeneratePathPrefix2' => false,
			],
			[
				'fileUploadMockParams' => $fileUploadMockParams,
				'filePath1' => '/a/a/a/a.jpg',
				'autogeneratePathPrefix1' => false,
				'filePath2' => '',
				'autogeneratePathPrefix2' => false,
			],
			[
				'fileUploadMockParams' => $fileUploadMockParams,
				'filePath1' => '',
				'autogeneratePathPrefix1' => true,
				'filePath2' => '',
				'autogeneratePathPrefix2' => false,
			],
			[
				'fileUploadMockParams' => $fileUploadMockParams,
				'filePath1' => '',
				'autogeneratePathPrefix1' => false,
				'filePath2' => '/a/a/a/a.jpg',
				'autogeneratePathPrefix2' => false,
			],
			[
				'fileUploadMockParams' => $fileUploadMockParams,
				'filePath1' => '',
				'autogeneratePathPrefix1' => false,
				'filePath2' => '',
				'autogeneratePathPrefix2' => true,
			],
			[
				'fileUploadMockParams' => $fileUploadMockParams,
				'filePath1' => '/a/a/a/a.jpg',
				'autogeneratePathPrefix1' => true,
				'filePath2' => '',
				'autogeneratePathPrefix2' => false,
			],
			[
				'fileUploadMockParams' => $fileUploadMockParams,
				'filePath1' => '/a/a/a/a.jpg',
				'autogeneratePathPrefix1' => false,
				'filePath2' => '/a/a/a/a.jpg',
				'autogeneratePathPrefix2' => false,
			],
			[
				'fileUploadMockParams' => $fileUploadMockParams,
				'filePath1' => '/a/a/a/a.jpg',
				'autogeneratePathPrefix1' => false,
				'filePath2' => '',
				'autogeneratePathPrefix2' => true,
			],
			[
				'fileUploadMockParams' => $fileUploadMockParams,
				'filePath1' => '',
				'autogeneratePathPrefix1' => true,
				'filePath2' => '/a/a/a/a.jpg',
				'autogeneratePathPrefix2' => false,
			],
			[
				'fileUploadMockParams' => $fileUploadMockParams,
				'filePath1' => '',
				'autogeneratePathPrefix1' => true,
				'filePath2' => '',
				'autogeneratePathPrefix2' => true,
			],
			[
				'fileUploadMockParams' => $fileUploadMockParams,
				'filePath1' => '',
				'autogeneratePathPrefix1' => false,
				'filePath2' => '/a/a/a/a.jpg',
				'autogeneratePathPrefix2' => true,
			],
			[
				'fileUploadMockParams' => $fileUploadMockParams,
				'filePath1' => '/a/a/a/a.jpg',
				'autogeneratePathPrefix1' => true,
				'filePath2' => '/a/a/a/a.jpg',
				'autogeneratePathPrefix2' => false,
			],
			[
				'fileUploadMockParams' => $fileUploadMockParams,
				'filePath1' => '/a/a/a/a.jpg',
				'autogeneratePathPrefix1' => true,
				'filePath2' => '',
				'autogeneratePathPrefix2' => true,
			],
			[
				'fileUploadMockParams' => $fileUploadMockParams,
				'filePath1' => '/a/a/a/a.jpg',
				'autogeneratePathPrefix1' => false,
				'filePath2' => '/a/a/a/a.jpg',
				'autogeneratePathPrefix2' => true,
			],
			[
				'fileUploadMockParams' => $fileUploadMockParams,
				'filePath1' => '',
				'autogeneratePathPrefix1' => true,
				'filePath2' => '/a/a/a/a.jpg',
				'autogeneratePathPrefix2' => true,
			],
			[
				'fileUploadMockParams' => $fileUploadMockParams,
				'filePath1' => '/a/a/a/a.jpg',
				'autogeneratePathPrefix1' => true,
				'filePath2' => '/a/a/a/a.jpg',
				'autogeneratePathPrefix2' => true,
			],
		];
	}

	/**
	 * @dataProvider addMultipleFileUploadArgs
	 */
	public function testAddMultipleFileUpload(
		bool $autogeneratePathPrefix,
		string $expectedFilePath1,
		string $expectedFilePath2,
	): void
	{
		$fileUpload1 = $this->getFileUploadMock(
			getUntrustedNameRetVal: 'img1.jpg',
			isImageRetVal: true,
			getUntrustedNameTimes: 1,
			isImageTimes: 0,
		);

		$fileUpload2 = $this->getFileUploadMock(
			getUntrustedNameRetVal: 'img2.jpg',
			isImageRetVal: true,
			getUntrustedNameTimes: 1,
			isImageTimes: 0,
		);

		$files = [
			$fileUpload1,
			$fileUpload2,
		];

		$this->fileUploadHolder->addMultipleFileUpload(
			files: $files,
			autogeneratePathPrefix: $autogeneratePathPrefix,
		);

		Assert::same([
			'img1.jpg' => [
				'file' => $fileUpload1,
				'filePath' => $expectedFilePath1,
			],
			'img2.jpg' => [
				'file' => $fileUpload2,
				'filePath' => $expectedFilePath2,
			],
		], $this->fileUploadHolder->getContent());
	}

	/**
	 * @return array<array<bool|string>>
	 */
	public function addMultipleFileUploadArgs(): array
	{
		$datePrefix = DateTime::from('now')->format('/Y/m/d/');

		return [
			[
				'autogeneratePathPrefix' => false,
				'expectedFilePath1' => 'img1.jpg',
				'expectedFilePath2' => 'img2.jpg',
			],
			[
				'autogeneratePathPrefix' => true,
				'expectedFilePath1' => $datePrefix . 'img1.jpg',
				'expectedFilePath2' => $datePrefix . 'img2.jpg',
			],
		];
	}

	/**
	 * @dataProvider addMultipleFileUploadInvalidArgumentExceptionArgs
	 */
	public function testAddMultipleFileUploadInvalidArgumentException(
		bool $autogeneratePathPrefix,
	): void
	{
		$fileUpload1 = $this->getFileUploadMock(
			getUntrustedNameRetVal: 'img1.jpg',
			isImageRetVal: true,
			getUntrustedNameTimes: 1,
			isImageTimes: 0,
		);

		$fileUpload2 = $this->getFileUploadMock(
			getUntrustedNameRetVal: 'img1.jpg',
			isImageRetVal: true,
			getUntrustedNameTimes: 1,
			isImageTimes: 0,
		);

		$files = [
			$fileUpload1,
			$fileUpload2,
		];

		$fileUploadHolder = $this->fileUploadHolder;

		Assert::exception(static function () use ($fileUploadHolder, $files, $autogeneratePathPrefix): void {
			$fileUploadHolder->addMultipleFileUpload(
				files: $files,
				autogeneratePathPrefix: $autogeneratePathPrefix,
			);
		}, InvalidArgumentException::class, 'Files must have unique name.', 516_154);
	}

	/**
	 * @return array<array<bool>>
	 */
	public function addMultipleFileUploadInvalidArgumentExceptionArgs(): array
	{
		return [
			[
				'autogeneratePathPrefix' => false,
			],
			[
				'autogeneratePathPrefix' => true,
			],
		];
	}

	public function testContainsOnlyImages(): void
	{
		$fileUpload = $this->getFileUploadMock(
			getUntrustedNameRetVal: 'img1.jpg',
			isImageRetVal: true,
			getUntrustedNameTimes: 1,
			isImageTimes: 1,
		);

		$this->fileUploadHolder->addFileUpload($fileUpload);

		Assert::same([], $this->fileUploadHolder->containsOnlyImages());
	}

	public function testContainsOnlyImages2(): void
	{
		$fileUpload = $this->getFileUploadMock(
			getUntrustedNameRetVal: 'img1.jpg',
			isImageRetVal: false,
			getUntrustedNameTimes: 2,
			isImageTimes: 1,
		);

		$this->fileUploadHolder->addFileUpload($fileUpload);

		Assert::same([
			'img1.jpg' => $fileUpload,
		], $this->fileUploadHolder->containsOnlyImages());
	}

	//////////// MOCKS:

	public function getFileUploadMock(
		string $getUntrustedNameRetVal,
		bool $isImageRetVal,
		int $getUntrustedNameTimes,
		int $isImageTimes,
	): FileUpload
	{
		$retVal = Mockery::mock(FileUpload::class)
			->shouldReceive('getUntrustedName')
			->with()
			->times($getUntrustedNameTimes)
			->andReturn($getUntrustedNameRetVal)
			->shouldReceive('isImage')
			->with()
			->times($isImageTimes)
			->andReturn($isImageRetVal)
			->getMock();
		assert($retVal instanceof FileUpload);

		return $retVal;
	}

}

(new FileUploadHolderTest())->run();
