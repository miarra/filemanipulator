# Quickstart
This extension comunicate with fileserver where you store images. For properly function You will need to run FileServer aplication which you can find at `https://bitbucket.org/miarra/fileserver.git`.

## Instalation

#### Download
The best way to install `miarra/filemanipulator`, is using Composer:
```sh
$ composer require miarra/filemanipulator
```

#### Registering
You can enable the extension using your neon config:
```sh
extensions:
	FileManipulator: Miarra\FileManipulator\FileManipulatorExtension
```

#### Injecting
You can simply inject factory in Your Presenters/Services:
```php
public function __construct(
	private \Miarra\FileManipulator\FileManipulator $fileManipulator,
) {
    parent::__construct();
    ...
}
```

#### HTACCESS
You must set RewriteRule in your .htaccess file like this:
```sh
	RewriteRule (.*\.(gif|jpg|jpeg|png|webp|bmp))$ https://urlOfYourFileserver.cz/images/$1 [P]
```

If your hosting doesn't support apache mod_proxy, you must use forward instead:
```sh
	RewriteRule (.*\.(gif|jpg|jpeg|png|webp|bmp))$ https://urlOfYourFileserver.cz/images/$1 [R=301,L]
```
Alternatively you can use php redirect or forward.

More info: https://httpd.apache.org/docs/2.4/rewrite/flags.html#flag_p

Info about enabling apache proxy: https://www.digitalocean.com/community/tutorials/how-to-use-apache-as-a-reverse-proxy-with-mod_proxy-on-ubuntu-16-04


## Setting
You must set options in config file like this:
```php
FileManipulator:
	url: 'http://urlOfYoursFileManipulatorServer.cz/'
	username: 'yourUsernameToFileManipulatorServer'
	password: 'yourPasswordToFileManipulatorServer'
```

# Saving, Loading and Deleting images

## Saving
For saving images you need use object of FileUploadHolder. You can get this object from factory method which is placed in FileManipulator (`createFileUploadHolder()`). 

### FileUploadHolder API
`addFileUpload(FileUpload $file, string $filePath = null, bool $autogeneratePathPrefix = false)` - Add single file. Default file path is its file name.
$filePath param contains location where you want to save your file. 
If the param $autogeneratePathPrefix is set to true $filePath params prefix is also set according to current date in `/Y/m/d/` format.

`addMultipleFileUpload(array $files, bool $autogeneratePathPrefix = false)` - Add multiple files into inner array. Default file path is its file name.
If the param $autogeneratePathPrefix is set to true $filePath params prefix is set according to current date in `/Y/m/d/` format.

`clear()` - Remove all file added to FileUploadHolder instance.

### 
When you fill FileUploadHolder you can call function `$returnedUrls = $this->fileManipulator->saveImages(FileUploadHolder $files)` which returns array of base urls of upload images, 
so in `$returnedUrls` will be `["fileUrl1", "fileUrl2"]` which would be compatible with your filePath specified in FileUploadHolder.

## Loading
If you want to get saved image in diffrent size then original, you need to send query parametres width or height (you can use one or both).

In case you don't use PROXY, you need concatenate returned URL with images URL path.

(Example: 'https://< yourFileServerURL.xy >/images/< yourReturnedURL >').

### Examples:
Original image: `$returnedUrls[$index]`

Image with width 250px: `$returnedUrls[$index] . '?width=250'`

Image with height 500px: `$returnedUrls[$index] . '?height=500'`

Image with max width 600px and max height 700px: `$returnedUrls[$index] . '?width=600&height=700'`


## Deleting
For delete images call function `$this->fileManipulator->deleteImages($returnedUrls)`.


## Copy
For copy images call function `$this->fileManipulator->copyImages($returnedUrls)`.

## Conclusion
This extension requires Nette3.0 and it is property of Miarra © 2021
