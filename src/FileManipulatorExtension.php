<?php declare(strict_types = 1);

namespace Miarra\FileManipulator;

use Nette\DI\CompilerExtension;
use Nette\DI\Helpers;
use Nette\Schema\Expect;
use Nette\Schema\Schema;

class FileManipulatorExtension extends CompilerExtension
{

	public function getConfigSchema(): Schema
	{
		return Expect::structure([
			'url' => Expect::string()->required(),
			'username' => Expect::string()->required(),
			'password' => Expect::string()->required(),
		])->castTo('array');
	}

	public function loadConfiguration(): void
	{
		$this->compiler->loadDefinitionsFromConfig(
			Helpers::expand(
				$this->loadFromFile(__DIR__ . '/config/common.neon')['services'],
				(array) $this->config,
			),
		);
	}

}
