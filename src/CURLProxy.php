<?php declare(strict_types = 1);

namespace Miarra\FileManipulator;

use CURLFile;

class CURLProxy
{

	public function init(): CURLBuilder
	{
		return new CURLBuilder();
	}

	public function createCURLFile(
		string $filename,
		string|null $mime_type = null,
		string|null $posted_filename = null,
	): CURLFile
	{
		return new CURLFile($filename, $mime_type, $posted_filename);
	}

}
