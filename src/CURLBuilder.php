<?php declare(strict_types = 1);

namespace Miarra\FileManipulator;

use CurlHandle;
use function curl_close;
use function curl_exec;
use function curl_init;
use function curl_setopt;

class CURLBuilder
{

	private CurlHandle $curl;

	public function __construct()
	{
		$this->curl = curl_init();
	}

	public function setopt(int $option, mixed $value): self
	{
		curl_setopt($this->curl, $option, $value);

		return $this;
	}

	public function exec(): bool|string
	{
		return curl_exec($this->curl);
	}

	public function close(): void
	{
		curl_close($this->curl);
	}

	public function __destruct()
	{
		$this->close();
	}

}
