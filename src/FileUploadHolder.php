<?php declare(strict_types = 1);

namespace Miarra\FileManipulator;

use Exception;
use Nette\Http\FileUpload;
use Nette\InvalidArgumentException;
use Nette\Utils\DateTime;
use function assert;

class FileUploadHolder
{

	/** @var array<array<string, FileUpload|string>> */
	private array $fileUploads = [];

	/**
	 * @param string|null $filePath Path to access image, must be in format of image name. Example: "/folder1/folder2/image.jpg"
	 * (in a collision fileserver add timestamp)
	 * @param bool $autogeneratePathPrefix Bool variable. If true - generates file path prefix from timestamp in format "/Y/m/d/"
	 * @param string|null $filePathPrefix Prefix to path to access image. Example: "/folder1/folder2/"
	 * @throws InvalidArgumentException|Exception
	 */
	public function addFileUpload(
		FileUpload $file,
		string|null $filePath = null,
		bool $autogeneratePathPrefix = false,
		string|null $filePathPrefix = null,
	): void
	{
		$fileName = $file->getUntrustedName();

		if (!$filePath) {
			$filePath = $fileName;
		}

		if ($autogeneratePathPrefix) {
			$filePath = DateTime::from('now')->format('/Y/m/d/') . $filePath;
		}

		if ($filePathPrefix) {
			$filePath = $filePathPrefix . '/' . $filePath;
		}

		if (isset($this->fileUploads[$fileName])) {
			throw new InvalidArgumentException('Files must have unique name.', 516_154);
		}

		$this->fileUploads[$fileName] = [
			'file' => $file,
			'filePath' => $filePath,
		];
	}

	/**
	 * @param array<FileUpload> $files
	 * @param bool $autogeneratePathPrefix Bool variable. If true - generates file path prefix from timestamp in format "/Y/m/d/"
	 * @param string|null $filePathPrefix Prefix to path to access image. Example: "/folder1/folder2/"
	 * @throws InvalidArgumentException|Exception
	 */
	public function addMultipleFileUpload(
		array $files,
		bool $autogeneratePathPrefix = false,
		string|null $filePathPrefix = null,
	): void
	{
		foreach ($files as $file) {
			$this->addFileUpload(
				file: $file,
				autogeneratePathPrefix: $autogeneratePathPrefix,
				filePathPrefix: $filePathPrefix,
			);
		}
	}

	public function clear(): void
	{
		$this->fileUploads = [];
	}

	/**
	 * @return array<array<string, FileUpload|string>>
	 */
	public function getContent(): array
	{
		return $this->fileUploads;
	}

	/**
	 * @return array<FileUpload> Array of FileUploads which aren't images
	 */
	public function containsOnlyImages(): array
	{
		$retArr = [];

		foreach ($this->fileUploads as $fileArr) {
			$file = $fileArr['file'];
			assert($file instanceof FileUpload);
			if (!$file->isImage()) {
				$retArr[$file->getUntrustedName()] = $file;
			}
		}

		return $retArr;
	}

}
