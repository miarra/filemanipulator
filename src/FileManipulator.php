<?php declare(strict_types = 1);

namespace Miarra\FileManipulator;

use CURLFile;
use Exception;
use Nette\Http\FileUpload;
use Nette\Http\IRequest as Request;
use Nette\InvalidArgumentException;
use Nette\Schema\Expect;
use Nette\Schema\Processor;
use Nette\Schema\ValidationException;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use stdClass;
use function array_keys;
use function array_values;
use function assert;
use function implode;
use function is_string;
use function md5;
use const CURLOPT_CUSTOMREQUEST;
use const CURLOPT_HTTPHEADER;
use const CURLOPT_POSTFIELDS;
use const CURLOPT_RETURNTRANSFER;
use const CURLOPT_URL;
use const CURLOPT_USERPWD;

class FileManipulator
{

	private const
		SAVE = 'save',
		DELETE = 'delete',
		COPY = 'copy',
				IMAGE_REGEX = '/.*\.(gif|jpe?g|bmp|png|webp)$/';

	/** @var array<string> */
	private array $methods = [
		self::SAVE => Request::POST,
		self::DELETE => Request::DELETE,
		self::COPY => Request::POST,
	];

	public function __construct(
		private CURLProxy $curlProxy,
		private string $url,
		private string $username,
		private string $password,
	)
	{
	}

	private function getCredentials(): string
	{
		return $this->username . ':' . $this->password;
	}

	public function createFileUploadHolder(): FileUploadHolder
	{
		return new FileUploadHolder();
	}

	/**
	 * @return array<string>
	 * @throws Exception
	 * @throws InvalidArgumentException
	 */
	public function saveImages(FileUploadHolder $files): array
	{
		if (!empty($notImageFiles = $files->containsOnlyImages())) {
			throw new InvalidArgumentException(
				'Function SaveImages accept only array of images. "' . implode(
					'", "',
					array_keys($notImageFiles),
				) . '".',
				654_111,
			);
		}

		$postData = $this->buildDataFiles($files->getContent());
		$resp = $this->getCurl(method: self::SAVE, postData: $postData);

		return $resp->filepath;
	}

	/**
	 * @param array<string>|string $filePaths
	 * @return array<string>
	 */
	public function deleteImages(array|string $filePaths): array
	{
			return $this->processCopyDelete($filePaths, self::DELETE);
	}

	/**
	 * @param array<string>|string $filePaths
	 * @return array<string>
	 */
	public function copyImages(array|string $filePaths): array
	{
			return $this->processCopyDelete($filePaths, self::COPY);
	}

	/**
	 * @param array<string>|string $filePaths
	 * @return array<string>
	 * @throws JsonException
	 * @throws ValidationException
	 */
	private function processCopyDelete(array|string $filePaths, string $method): array
	{
		if (is_string($filePaths)) {
			$filePaths = [$filePaths];
		}

		$schema = Expect::arrayOf(Expect::string());
		$processor = new Processor();
		$processor->process($schema, $filePaths);

		$resp = $this->getCurl(
			method: $method,
			headers: ['Content-Type: application/json;'],
			postData: Json::encode($filePaths),
		);

		return $resp->filepath;
	}

	/**
	 * @param array<string>|null $headers
	 * @param string|array<string, CURLFile|string>|null $postData
	 * @throws JsonException
	 */
	private function getCurl(
		string $method,
		array|null $headers = null,
		string|array|null $postData = null,
	): stdClass
	{
		$url = $this->url . '/images/' . $method;

		$curl = $this->curlProxy
			->init()
			->setopt(CURLOPT_URL, $url)
			->setopt(CURLOPT_USERPWD, $this->getCredentials())
			->setopt(CURLOPT_CUSTOMREQUEST, $this->methods[$method])
			->setopt(CURLOPT_RETURNTRANSFER, true);

		if ($headers && !empty($headers)) {
			$curl->setopt(CURLOPT_HTTPHEADER, $headers);
		}

		if ($postData && !empty($postData)) {
			$curl->setopt(CURLOPT_POSTFIELDS, $postData);
		}

		$response = $curl->exec();
		$curl->close();
		$resp = Json::decode(is_string($response) ? $response : '');

		if ($resp->state !== 'SUCCESS') {
			throw new Exception($resp->message ?? '', $resp->code);
		}

		return $resp;
	}

	/**
	 * @param array<array<string, FileUpload|string>> $files
	 * @return array<string, CURLFile|string>
	 */
	private function buildDataFiles(array $files): array
	{
		$postData = [];
		foreach (array_values($files) as $key => $fileArray) {
			$file = $fileArray['file'];
			assert($file instanceof FileUpload);
			$fileName = $file->getUntrustedName();
			$postData['upload_file_' . $key] = $this->curlProxy->createCURLFile(
				$file->getTemporaryFile(),
				$file->getContentType(),
				$fileName,
			);
			$filePath = $fileArray['filePath'];
			assert(is_string($filePath));
			$postData[md5($fileName)] = $filePath;
		}

		return $postData;
	}

}
